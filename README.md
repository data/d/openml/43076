# OpenML dataset: 1DUltrasoundMuscleFatigueDataStudy2Of2

https://www.openml.org/d/43076

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This collection includes data sets of one-dimensional ultrasound raw RF data (A-Scans) acquired from the biceps brachii muscles of a single healthy volunteer. The annotation was performed by labeling the first 10 seconds of each data set as belonging to a relaxed muscle state and the last 10 seconds of each data set as belonging to a fatigue muscle states. Each line of the ARFF file contains an A-Scan consisting of 2048amplitude values, an annotation (whether this A-Scan belongs to a fatigue muscle state (1) or not (0)), a timestamp, the arm the signals were obtained from, the ID of the subject, the ID of the data set, the gender of the subject and the chosen lifted weight (2.5. kg, 5.0 kg or 7.5 kg).

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43076) of an [OpenML dataset](https://www.openml.org/d/43076). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43076/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43076/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43076/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

